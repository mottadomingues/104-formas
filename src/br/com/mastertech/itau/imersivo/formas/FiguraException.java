package br.com.mastertech.itau.imersivo.formas;

public class FiguraException extends Exception{

	private static final long serialVersionUID = 1L;

	public FiguraException()
	{
		super("ERRO: Quantidade de lados superior a figura desejada");
	}
}
