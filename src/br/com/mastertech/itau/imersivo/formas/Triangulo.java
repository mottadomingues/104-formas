package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Triangulo extends Figura{

	public Triangulo(LadoLista lados) throws Exception {
		super(lados);
		
		if(lados.size() != 3)
			throw new FiguraException();
	}

	@Override
	public double calcularArea() throws Exception{
		double ladoA = lados.get(0);
		double ladoB = lados.get(1);
		double ladoC = lados.get(2);
		if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA)
		{
			double s = (ladoA + ladoB + ladoC) / 2;       
			double area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
			return area;	
		}
		else
		{
			throw new Exception("Mas o triangulo informado era inválido :/");
		}
		
	}

	@Override
	public String toString() {
		return "triângulo";
	}

}
