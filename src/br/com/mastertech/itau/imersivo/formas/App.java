package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		Impressora.imprimir("Olá! bem vindo ao calculador de área 3 mil!");
		Impressora.imprimir("Basta informar a medida de cada lado que eu te digo a área :)");
		Impressora.imprimir("Vamos começar!");
		Impressora.imprimir("");
		Impressora.imprimir("Obs: digite -1 se quiser parar de cadastrar lados!");
		Impressora.imprimir("");
		
		LadoLista lados = new LadoLista();
		
		boolean deveAdicionarNovoLado = true;
		while(deveAdicionarNovoLado) {
			Impressora.imprimir("Informe o tamanho do lado " + (lados.size() + 1));
			
			double tamanhoLado = Double.parseDouble(scanner.nextLine());
			
			if(tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			}else {
				lados.add(tamanhoLado);
			}
		}
		
		Impressora.imprimir("Lados cadastrados!");
		Impressora.imprimir("Agora vamos calcular a área...");
		
		Figura objFigura = null;
		try {
			objFigura = Figura.criarFigura(lados);
			Impressora.imprimir("Eu identifiquei um " + objFigura + "!");
			Impressora.imprimir("A área do " + objFigura + " é " + objFigura.calcularArea());
		} catch (Exception e) {
			Impressora.imprimir(e.getMessage());
			return;
		}
		
	}

}
