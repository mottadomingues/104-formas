package br.com.mastertech.itau.imersivo.formas;

public class Circulo extends Figura{
	
	public Circulo(LadoLista lados) throws Exception {
		super(lados);
		
		if(lados.size() != 1)
			throw new FiguraException();
	}

	@Override
	public double calcularArea() {
		return (Math.PI * Math.pow(lados.get(0), 2));
	}

	@Override
	public String toString() {
		return "círculo";
	}

}
