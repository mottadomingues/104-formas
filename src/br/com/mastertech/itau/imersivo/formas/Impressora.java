package br.com.mastertech.itau.imersivo.formas;

public class Impressora {
	public static String SEPARADOR_LINHA = "-------------------------------------------------------------";
	public static void imprimir(Object valor){
		System.out.println(valor);
	}
	
	public static void imprimirComSeparadorLinha(Object valor){
		Impressora.imprimir(valor + "\n" + SEPARADOR_LINHA);
	}
}
