package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

public abstract class Figura
{
	protected LadoLista lados = new LadoLista();
	
	public Figura()
	{
		
	}
	
	public Figura(LadoLista lados) throws Exception
	{
		this.lados = lados;
	}
	
	public LadoLista getLados()
	{
		return lados;
	}
	
	public int getQuantidadeLados()
	{
		return lados.size();
	}
	
	abstract public double calcularArea() throws Exception;

	@Override
	abstract public String toString();
	
	public static Figura criarFigura(LadoLista lados) throws Exception
	{
		int qtdLados = lados.size();
		
		if(qtdLados == 0) {
			throw new Exception("Forma inválida!");
		}else if(qtdLados == 1) {
			return new Circulo(lados);
		}else if(qtdLados == 2) {
			return new Quadrilatero(lados);
		}else if(qtdLados == 3) {
			return new Triangulo(lados);
		}else {
			throw new Exception("Ops! Eu não conheço essa forma geometrica ¯\\_(⊙_ʖ⊙)_/¯");
		}
	}
}
