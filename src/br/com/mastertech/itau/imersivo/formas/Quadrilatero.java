package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Quadrilatero  extends Figura{

	public Quadrilatero(LadoLista lados) throws Exception {
		super(lados);
		
		if(lados.size() != 2)
			throw new FiguraException();
	}

	@Override
	public double calcularArea() {
		return (lados.get(0) * lados.get(0));
	}

	@Override
	public String toString() {
		return "quadrado/retangulo";
	}

}
