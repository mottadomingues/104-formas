package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

public class LadoLista{

	protected List<Double> lados = new ArrayList<>();
	
	public void add(double tamanhoLado) {
		lados.add(tamanhoLado);
	}

	public double get(int index) {
		return lados.get(index);
	}

	public int size() {
		return lados.size();
	}
}
